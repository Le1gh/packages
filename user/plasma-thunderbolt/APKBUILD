# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=plasma-thunderbolt
pkgver=5.24.5
pkgrel=0
pkgdesc="Thunderbolt device integration for KDE Plasma desktop"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires running D-Bus.
license="GPL-2.0-only"
depends="bolt"
makedepends="cmake extra-cmake-modules kcmutils-dev kcoreaddons-dev
	knotifications-dev qt5-qtbase-dev qt5-qtdeclarative-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-thunderbolt-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b1096fac09800e33b87e79a2e774eaf19afa063263e71f772ec0081fda714c63dd07a0000489c36c3f7deead3ee563880bc12f336b8ea599b396613d28cc08f5  plasma-thunderbolt-5.24.5.tar.xz"
