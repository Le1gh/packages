# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Andrew Manison <amanison@anselsystems.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=mutt
pkgver=2.2.10
pkgrel=0
pkgdesc="Small but very powerful text-mode email client"
url="http://www.mutt.org"  # No HTTPS
arch="all"
options="suid !check"  # No test suite.
license="GPL-2.0+ AND LGPL-2.0+ AND LGPL-2.1+ AND Public-Domain"
depends=""
makedepends="cyrus-sasl-dev gdbm-dev gettext-tiny gpgme-dev
	libidn-dev ncurses-dev openssl-dev perl"
subpackages="$pkgname-doc $pkgname-lang"
source="https://bitbucket.org/$pkgname/$pkgname/downloads/$pkgname-$pkgver.tar.gz"

# secfixes:
#   1.14.4-r0:
#     - CVE-2020-14093

build() {
	ISPELL=/usr/bin/hunspell \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--enable-imap \
		--enable-pop \
		--enable-smtp \
		--enable-hcache \
		--enable-gpgme \
		--enable-sidebar \
		--enable-smime \
		--with-curses \
		--with-mailpath=/var/spool/mail \
		--with-docdir=/usr/share/doc/$pkgname \
		--with-ssl \
		--with-sasl
	make
}

package() {
	make DESTDIR="$pkgdir" install

	rm "$pkgdir"/etc/*.dist \
		"$pkgdir"/etc/mime.types \
		"$pkgdir"/usr/bin/muttbug \
		"$pkgdir"/usr/bin/flea

	# Don't tamper with the global configuration file.
	# Many options set in the global config cannot be
	# overwritten in the users configuration file.
	# Example: Resetting colors isn't possible.
	install -Dm644 contrib/gpg.rc \
		"$pkgdir"/etc/Muttrc.gpg.dist
}

sha512sums="6db0009163b31425dafe7b6e38374a623f1fd057daf57baaebe18be6d42e5a0ce7087a41bc5e2411f4fbe144497a43682c480754b51108952c7dd16b9294192f  mutt-2.2.10.tar.gz"
