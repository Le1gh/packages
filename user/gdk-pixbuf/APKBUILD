# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gdk-pixbuf
pkgver=2.42.12
pkgrel=0
pkgdesc="GTK+ image loading library"
url="https://www.gtk.org/"
arch="all"
options="!check"  # bug753605-atsize.jpg is missing from tarball.
license="LGPL-2.0+"
depends="shared-mime-info"
makedepends="glib-dev gobject-introspection-dev libjpeg-turbo-dev libpng-dev
	meson python3 py3-docutils tiff-dev xmlto"
install="$pkgname.pre-deinstall"
triggers="$pkgname.trigger=/usr/lib/gdk-pixbuf-2.0/*/loaders"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.gnome.org/sources/gdk-pixbuf/${pkgver%.*}/gdk-pixbuf-$pkgver.tar.xz"

# secfixes:
#   2.36.6-r1:
#     - CVE-2017-6311
#     - CVE-2017-6312
#     - CVE-2017-6314

build() {
	meson setup \
		-Dprefix=/usr \
		-Dinstalled_tests=false \
		build
	meson compile -C build
}

check() {
	meson test -C build
}

package() {
	DESTDIR="$pkgdir" meson install -C build
}

sha512sums="ae9fcc9b4e8fd10a4c9bf34c3a755205dae7bbfe13fbc93ec4e63323dad10cc862df6a9e2e2e63c84ffa01c5e120a3be06ac9fad2a7c5e58d3dc6ba14d1766e8  gdk-pixbuf-2.42.12.tar.xz"
