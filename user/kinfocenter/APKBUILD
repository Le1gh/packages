# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=kinfocenter
pkgver=5.24.5
pkgrel=0
pkgdesc="Information about the running computer"
url="https://userbase.kde.org/KInfoCenter"
arch="all"
license="GPL-2.0-only"
depends="kirigami2 mesa-demos pciutils systemsettings xdpyinfo"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kcompletion-dev ki18n-dev
	kconfig-dev kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev kio-dev
	kdoctools-dev kiconthemes-dev kcmutils-dev kdelibs4support-dev glu-dev
	kservice-dev solid-dev kwidgetsaddons-dev kxmlgui-dev kdeclarative-dev
	kirigami2-dev kpackage-dev libraw1394-dev libusb-dev pciutils-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/kinfocenter-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="598d7993808ac6f32ae89c2a50624caf81c4d7a355ae155469d79fda923d3bcd4256077863186a42d594ddd36c06ffb5c3fd02987cf638b2bc88dffdb9b47883  kinfocenter-5.24.5.tar.xz"
