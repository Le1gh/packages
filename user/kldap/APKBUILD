# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kldap
pkgver=22.04.2
pkgrel=0
pkgdesc="KDE LDAP library"
url="https://kde.org/"
arch="all"
license="BSD-3-Clause AND CC0-1.0 AND LGPL-2.0+ AND MIT"
depends=""
makedepends="qt5-qtbase-dev cmake extra-cmake-modules cyrus-sasl-dev kauth-dev
	kcodecs-dev kcoreaddons-dev kdoctools-dev ki18n-dev kio-dev kwallet-dev
	kjobwidgets-dev kservice-dev kwidgetsaddons-dev openldap-dev solid-dev
	qtkeychain-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kldap-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="2cff2279b059c62f3acfbff9a7f9b219bd605b78ccc710668f7a87b5fa607ffe887936cda9b9a5b5f2d2e44419f32b7ab0a39cbf7d4baac810802df6dd676d35  kldap-22.04.2.tar.xz"
