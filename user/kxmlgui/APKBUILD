# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kxmlgui
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework for creating user interfaces using XML"
url="https://www.kde.org/"
arch="all"
options="!check"  # Test suite requires OpenGL-accelerated X11 session
license="LGPL-2.1-only AND LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev kcoreaddons-dev kitemviews-dev kconfig-dev
	kconfigwidgets-dev kiconthemes-dev ktextwidgets-dev kglobalaccel-dev"
docdepends="kcoreaddons-doc kauth-doc kwidgetsaddons-doc kcodecs-doc
	kconfig-doc kconfigwidgets-doc"
makedepends="$depends_dev cmake extra-cmake-modules doxygen graphviz
	qt5-qttools-dev $docdepends"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kxmlgui-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="6f220a270263a97d4e6965360d0e8c908a0a99ae29cced7fd8a971d1f2ed9a6b180697ebb6fc93012ac3ea3f7b22c25539cd35c70ddf840ab2c0f63cd21e6bc5  kxmlgui-5.94.0.tar.xz"
