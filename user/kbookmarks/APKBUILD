# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kbookmarks
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework for managing XBEL-format bookmarks"
url="https://www.kde.org/"
arch="all"
options="!check"  # Test requires X11.
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev kconfig-dev kcoreaddons-dev kcodecs-dev
	kconfigwidgets-dev kiconthemes-dev kxmlgui-dev"
docdepends="kwidgetsaddons-doc"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen
	graphviz $docdepends"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kbookmarks-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make	
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="0ab54590cc2a26039804d792d8c6399257e4bfbd92b34cbe8043b1d6e02f102783c229a7e457acefd6da5876481af2558d026dd3e9d4234a6adce14c8aa6c8b3  kbookmarks-5.94.0.tar.xz"
