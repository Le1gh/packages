# Contributor: Brandon Bergren <git@bdragon.rtk0.net>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=py3-jinja2
_pkgname=Jinja2
_p="${_pkgname#?}"
_p="${_pkgname%"$_p"}"
pkgver=3.0.3
pkgrel=0
pkgdesc="A small but fast and easy to use stand-alone template engine written in pure python."
url="https://pypi.python.org/pypi/Jinja2"
arch="noarch"
license="BSD-3-Clause"
depends="python3 py3-markupsafe"
makedepends="python3-dev py3-iniconfig py3-toml"
checkdepends="py3-pytest"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/$_p/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

# secfixes: jinja2
#   2.10.1-r0:
#     - CVE-2019-10906

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$builddir/build/lib:$PYTHONPATH" pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="51703d396ffe35155ed216922294441e20d44cd1ab69674140146375d4964654cdb32b38945f22916e0de917bbc0cf406cb680c2e569f9225dbe1fe4063be0b0  py3-jinja2-3.0.3.tar.gz"
