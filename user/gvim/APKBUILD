# Contributor: Sergey Lukin <sergej.lukin@gmail.com>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=gvim
_pkgreal=vim
pkgver=9.1.0378
pkgrel=0
pkgdesc="advanced text editor"
url="http://www.vim.org"
arch="all"
options="!check"  # requires controlling TTY, and fails with musl locales
license="Vim"
depends="vim"
makedepends="acl-dev ncurses-dev libx11-dev perl-dev python3-dev gtk+3.0-dev
	libice-dev libsm-dev libxpm-dev libxt-dev"
source="$_pkgreal-$pkgver.tar.gz::https://github.com/$_pkgreal/$_pkgreal/archive/v$pkgver.tar.gz"
builddir="$srcdir/$_pkgreal-$pkgver"

# secfixes:
#   9.1.0148-r0:
#     - CVE-2023-4752
#     - CVE-2023-4781
#     - CVE-2023-5344
#     - CVE-2023-5441
#     - CVE-2023-5535
#     - CVE-2023-46246
#     - CVE-2023-48231
#     - CVE-2023-48232
#     - CVE-2023-48233
#     - CVE-2023-48234
#     - CVE-2023-48235
#     - CVE-2023-48236
#     - CVE-2023-48237
#     - CVE-2023-48706
#     - CVE-2024-22667
#   9.0.1385-r0:
#     - CVE-2023-0049
#     - CVE-2023-0051
#     - CVE-2023-0054
#     - CVE-2023-0288
#     - CVE-2023-0433
#     - CVE-2023-0512
#   8.0.0329-r0:
#     - CVE-2017-5953
#   8.0.0056-r0:
#     - CVE-2016-1248

prepare() {
	default_prepare
	# Read vimrc from /etc/vim
	echo '#define SYS_VIMRC_FILE "/etc/vim/vimrc"' >> src/feature.h
}

build() {
	local _onlynative
	[ "$CBUILD" != "$CHOST" ] || _onlynative="--enable-perlinterp --enable-python3interp=dynamic"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		$_onlynative \
		--with-x \
		--enable-acl \
		--enable-nls \
		--enable-multibyte \
		--enable-gui=gtk3 \
		--disable-ncurses \
		--with-compiledby="Adélie Linux" \
		vim_cv_toupper_broken=no \
		vim_cv_terminfo=yes \
		vim_cv_tgent=zero \
		vim_cv_tty_group=world \
		vim_cv_getcwd_broken=no \
		vim_cv_stat_ignores_slash=no \
		vim_cv_memmove_handles_overlap=yes \
		STRIP=:
	make
}

package() {
	install -Dm755 src/vim "$pkgdir"/usr/bin/gvim
	install -Dm755 src/gvimtutor "$pkgdir"/usr/bin/gvimtutor
	install -Dm644 runtime/vim16x16.png "$pkgdir"/usr/share/icons/locolor/16x16/apps/gvim.png
	install -Dm644 runtime/vim32x32.png "$pkgdir"/usr/share/icons/locolor/32x32/apps/gvim.png
	install -Dm644 runtime/vim48x48.png "$pkgdir"/usr/share/icons/hicolor/48x48/apps/gvim.png
	install -Dm644 runtime/gvim.desktop "$pkgdir"/usr/share/applications/gvim.desktop

	cd "$pkgdir"/usr/bin
	ln -s gvim gview
	ln -s gvim gvimdiff
	ln -s gvim rgview
	ln -s gvim rgvim
}

sha512sums="d1c31de1e39bd3504a462aa324b699bb81ff11717171529047e5518c9bb2aec651b0b40a18b66f4df92a1b2065abc68a523df61681f32228839e69ba0f383e6b  vim-9.1.0378.tar.gz"
