# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libjpeg-turbo
pkgver=2.0.5
pkgrel=0
pkgdesc="Accelerated JPEG compression and decompression library"
url="https://libjpeg-turbo.org/"
arch="all"
license="IJG AND BSD-3-Clause AND Zlib"
depends=""
makedepends="cmake"
subpackages="$pkgname-doc $pkgname-dev $pkgname-utils"
source="https://downloads.sourceforge.net/libjpeg-turbo/libjpeg-turbo-$pkgver.tar.gz
	"

case "$CTARGET_ARCH" in
pmmx | x86 | x86_64)	makedepends="$makedepends nasm" ;;
esac

# secfixes:
#   2.0.3-r0:
#     - CVE-2019-2201
#   2.0.4-r1:
#     - CVE-2020-13790

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi

	# https://github.com/libjpeg-turbo/libjpeg-turbo/issues/344
	# https://github.com/libjpeg-turbo/libjpeg-turbo/issues/428
	case "$CARCH" in
	ppc) _floattest=64bit;;
	esac

	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_DEFAULT_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DENABLE_STATIC=OFF \
		-DWITH_JPEG8=ON \
		${_floattest:+-DFLOATTEST="$_floattest"} \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

utils() {
	pkgdesc="Utilities for manipulating JPEG images"
	replaces="jpeg"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="5bf9ecf069b43783ff24365febf36dda69ccb92d6397efec6069b2b4f359bfd7b87934a6ce4311873220fccc73acabdacef5ce0604b79209eb1912e8ba478555  libjpeg-turbo-2.0.5.tar.gz"
