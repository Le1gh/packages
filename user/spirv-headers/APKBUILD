# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=spirv-headers
pkgver=1.3.290.0
pkgrel=0
pkgdesc="Headers for translation between SPIR-V and LLVM IR"
url="https://github.com/KhronosGroup/SPIRV-Headers"
arch="noarch"
license="MIT"
depends=""
makedepends="cmake"
subpackages=""
source="spirv-headers-$pkgver.tar.gz::https://github.com/KhronosGroup/SPIRV-Headers/archive/refs/tags/vulkan-sdk-$pkgver.tar.gz"
builddir="$srcdir/SPIRV-Headers-vulkan-sdk-$pkgver"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="d3328cd4ddf87d075afacfb7ada01dbd16a3ff39b831e9ebe4ce3c32af0ff0c8822811b0e0d273a54b4acaba29b63b099efcf0150424bd9074d24d04a9974d89  spirv-headers-1.3.290.0.tar.gz"
