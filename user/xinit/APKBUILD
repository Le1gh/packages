# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=xinit
pkgver=1.4.2
pkgrel=0
pkgdesc="X.Org initialisation program"
url="https://www.X.Org/"
arch="all"
license="X11"
depends="cmd:mcookie xauth xmodmap xrdb"
makedepends="libx11-dev util-macros"
subpackages="$pkgname-doc"
source="https://www.X.Org/releases/individual/app/xinit-$pkgver.tar.xz
	06_move_serverauthfile_into_tmp.patch
	xinitrc
	xsession.skel
	Xsession
	xserverrc"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--with-xinitdir=/etc/X11/xinit
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	chmod +x "$pkgdir"/usr/bin/startx
	install -m755 -d "$pkgdir"/etc/skel
	install -m755 -D "$srcdir"/xinitrc "$pkgdir"/etc/X11/xinit/xinitrc
	install -m755 -D "$srcdir"/Xsession "$pkgdir"/etc/X11/xinit/Xsession
	install -m755 "$srcdir"/xsession.skel "$pkgdir"/etc/skel/.xsession
	install -m755 "$srcdir"/xserverrc "$pkgdir"/etc/X11/xinit/xserverrc
	mkdir -p "$pkgdir"/etc/X11/xinit/xinitrc.d
}

sha512sums="4b62c2edd97b40133577cbba88b3f31b36c5634b4eb667ef0c302e8358dc1c55a255abe42aaadc910d8aa9ea0e3add157a12a301382f1cdbe091df4e1215fae0  xinit-1.4.2.tar.xz
2c4527cdb7d56b129266c7ba68018eb1b0b794a8a1c0b0e532b11afc243fca36e2f94595b79cf65b6e69d89640edaef94b14295d163cf49d076456ece654d9fb  06_move_serverauthfile_into_tmp.patch
1b19f800d1f64e49b533929ea6b9580d6dc24b680e1461edcc5784ba9fbb387fef57576b1e3d4bc67cb0ff2b248b670c148a3c7a350e1062bb9b95e9a2ba7805  xinitrc
448bc6c7987a4735b6584e307cd5c53092b1a338043293f5f110d11818dd1b80508401a3b6f09525c82a16a88f293b37011d8ca112460b0f95d26897e3e0619e  xsession.skel
b311032a751bb21d6c49ffe1dfc67beb577b5d5bec4a4c12612c4c0f9b9d6d2a07f7435c7d2fe9ab4c318546ee0cb9d4ff3f7ef908e756a818da529e913e667d  Xsession
f86d96d76bcb340021e7904925f0029f8662e4dfc32489198b3a8695dca069da496539e2287249c763fe9c4d8d5d591fd18fe49a0bee822cbbd0eb712efbb89b  xserverrc"
