# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=font-adobe-utopia-75dpi
pkgver=1.0.5
pkgrel=0
pkgdesc="75dpi Utopia X11 font from Adobe"
url="https://www.X.Org/"
arch="noarch"
# Okay.
# This is really hairy, but Fedora Legal says the TUG license can apply to the
# X11 distribution[1][2]; it's almost MIT style, but you have to rename the
# font if you modify it in any way.
# [1]: https://fedoraproject.org/wiki/Legal_considerations_for_fonts
# [2]: https://src.fedoraproject.org/cgit/rpms/xorg-x11-fonts.git/tree/xorg-x11-fonts.spec
license="Utopia"
depends="encodings font-alias fontconfig mkfontscale"
makedepends="bdftopcf font-util-dev util-macros"
subpackages=""
source="https://www.x.org/releases/individual/font/font-adobe-utopia-75dpi-$pkgver.tar.xz"

prepare() {
	default_prepare
	update_config_sub
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	find "$pkgdir" -name fonts.dir -exec rm {} +
}

sha512sums="9608698288fca47bf61cff911b8f081593807eed9f138ea22f05a7b6c26c481ec5144d79665cf509ba889df35ef5406e0de71710362bcf6de6b602c12158e22a  font-adobe-utopia-75dpi-1.0.5.tar.xz"
