# Contributor: Horst G. Burkhardt <horst@adelielinux.org>
# Maintainer: Horst G. Burkhardt <horst@adelielinux.org>
pkgname=fastfetch
pkgver=2.5.0
pkgrel=1
pkgdesc="Tool for fetching and presenting system information"
url="https://github.com/fastfetch-cli/fastfetch"
arch="all"
options="!check"  # No separate test suite available.
subpackages="$pkgname-doc"
license="MIT"
depends=""
makedepends="cmake dbus-dev dconf-dev libdrm-dev libxcb-dev libxrandr-dev
	mesa-dev networkmanager-dev pciutils-dev pulseaudio-dev sqlite-dev
	utmps-dev wayland-dev xfconf-dev zlib-dev libglvnd-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/fastfetch-cli/fastfetch/archive/refs/tags/$pkgver.tar.gz"

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DENABLE_DBUS=ON \
		-DENABLE_DCONF=ON \
		-DENABLE_DRM=ON \
		-DENABLE_LIBNM=ON \
		-DENABLE_LIBPCI=ON \
		-DENABLE_OSMESA=ON \
		-DENABLE_PULSE=ON \
		-DENABLE_WAYLAND=ON \
		-DENABLE_X11=ON \
		-DENABLE_XCB=ON \
		-DENABLE_XFCONF=ON \
		-DENABLE_XRANDR=ON \
		-DENABLE_ZLIB=ON
	make VERBOSE=1
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="a69f4e839acf8897a9cd8f6c017a4d7657a8dc0b3c58e645c88fc8e47e0a7bd3411d1a3e1e916628a948cb7e1688a200f7ac1eabd090f5eb42ce4b0030e342b4 fastfetch-2.5.0.tar.gz"
