#!/bin/sh
# Distributed under the terms of the BSD License.
# Copyright 1999-2011 Gentoo Foundation
# Written by Roy Marples <uberlord@gentoo.org>
# Copyright 2018 Adélie Linux.

if [ -z "$1" -o -z "$2" ]; then
	logger -t wpa_cli "Insufficient parameters"
	exit 1
fi

INTERFACE="$1"
ACTION="$2"

EXEC="/etc/init.d/net.${INTERFACE} --quiet"

case "${ACTION}" in
	CONNECTED)
		EXEC="${EXEC} start"
		;;
	DISCONNECTED)
		# Deactivated, since stopping /etc/init.d/net.wlanX
		# stops the network completly.
		EXEC="false ${EXEC} stop"
		;;
	*)
		logger -t wpa_cli "Unknown action '${ACTION}'"
		exit 1
		;;
esac

# netifrc can use ${IN_BACKGROUND} so that it knows that the user isn't
# stopping the interface and a background process - like wpa_cli - is.
export IN_BACKGROUND=true

logger -t wpa_cli "interface ${IFNAME} ${ACTION}"
${EXEC} || logger -t wpa_cli "executing '${EXEC}' failed"
