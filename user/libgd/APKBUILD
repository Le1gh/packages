# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=libgd
pkgver=2.3.3
pkgrel=1
pkgdesc="Library for dynamic image creation"
url="http://libgd.github.io/"
arch="all"
options="!check"  # Multiple test suite failures. Assumes SSE+ math.
license="MIT"
depends=""
makedepends="autoconf automake bash fontconfig-dev freetype-dev
	libjpeg-turbo-dev libpng-dev libtool libwebp-dev libxpm-dev tiff-dev
	zlib-dev
	"
# While the fontconfig/basic test checks for what happens if an empty
# fontlist is passed to gdImageStringFT(), there still needs to be at
# least one font installed on the system...
checkdepends="ttf-liberation"
subpackages="$pkgname-dev"
replaces="gd"
source="https://github.com/$pkgname/$pkgname/releases/download/gd-$pkgver/$pkgname-$pkgver.tar.xz
	revert-318-removal-of-macros.patch
	"

# secfixes:
#   2.3.3-r0:
#     - CVE-2019-11038
#     - CVE-2019-6977
#     - CVE-2019-6978
#   2.2.5-r1:
#     - CVE-2018-5711
#     - CVE-2018-1000222
#     - CVE-2019-6977
#     - CVE-2019-6978
#   2.2.5-r2:
#     - CVE-2018-14553
#   2.3.0-r0:
#     - CVE-2019-11038

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--with-xpm \
		--disable-werror
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

dev() {
	default_dev
	depends="$pkgname perl"
	replaces="gd-dev"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/bdftogd "$subpkgdir"/usr/bin
}

sha512sums="aa49d4381d604a4360d556419d603df2ffd689a6dcc10f8e5e1d158ddaa3ab89912f6077ca77da4e370055074007971cf6d356ec9bf26dcf39bcff3208bc7e6c  libgd-2.3.3.tar.xz
623e312e20f1994c6ae26f7fdac45b0eb7f4e65b83160ca4e22495c37b162b2dbde21ede6aab189e566b8934bb22aafdb80e9263cd87118013233927a2ab3601  revert-318-removal-of-macros.patch"
