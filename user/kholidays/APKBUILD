# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kholidays
pkgver=5.94.0
pkgrel=0
pkgdesc="List of national holidays for many countries"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1"
depends=""
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtbase-dev
	qt5-qtdeclarative-dev qt5-qttools-dev doxygen"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kholidays-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# Requires *actual* *locale* *support*!
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E testholidayregion
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="7c362a8ba852258c95c6eceb9f7dbd82fd3bdfdeab5c844c5efd772244c823f772616e68e67f40f3c641278eec65126b9ee599942b8393103600b24d97e9e77e  kholidays-5.94.0.tar.xz"
