From 500cbd7b90fa7eb5716a3bbc6aa788ada028a1bf Mon Sep 17 00:00:00 2001
From: Adam Cozzette <acozzette@google.com>
Date: Thu, 11 Aug 2022 18:22:15 -0700
Subject: [PATCH] Upgrade third_party/googletest submodule to current main
 branch (#10393)

* Upgrade third_party/googletest submodule to current main branch

We can finally do this upgrade now that we have dropped our autotools
build. Googletest recommends living at head, so let's go straight to the
most recent commit on main. For some reason the googletest archive is
not present in the Bazel build mirror, so I removed that entry and just
left the GitHub download link in our WORKSPACE file.

Googletest now requires C++14, so I updated all the C++11 flags I could
find to C++14 instead. I added a .bazelrc file to add -std=c++14 for all
our Bazel builds.

* Delete the empty //src/google/protobuf:protobuf_test target

* Avoid building C++ unit tests in aarch64 jobs for Python and Ruby
---
 .bazelrc                                            |  1 +
 CMakeLists.txt                                      |  8 ++++----
 WORKSPACE                                           |  7 +++----
 examples/Makefile                                   |  4 ++--
 python/setup.py                                     |  2 +-
 5 files changed, 14 insertions(+), 24 deletions(-)
 create mode 100644 .bazelrc

diff --git a/.bazelrc b/.bazelrc
new file mode 100644
index 00000000000..f93facf00a8
--- /dev/null
+++ b/.bazelrc
@@ -0,0 +1 @@
+build --cxxopt=-std=c++14
diff --git a/CMakeLists.txt b/CMakeLists.txt
index 345d7d05a49..b343c64e48e 100644
--- a/cmake/CMakeLists.txt
+++ b/cmake/CMakeLists.txt
@@ -32,11 +32,11 @@ if(protobuf_DEPRECATED_CMAKE_SUBDIRECTORY_USAGE)
   get_filename_component(protobuf_SOURCE_DIR ${protobuf_SOURCE_DIR} DIRECTORY)
 endif()
 
-# Add c++11 flags
+# Add c++14 flags
 if (CYGWIN)
-  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++11")
+  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++14")
 else()
-  set(CMAKE_CXX_STANDARD 11)
+  set(CMAKE_CXX_STANDARD 14)
   set(CMAKE_CXX_STANDARD_REQUIRED ON)
   set(CMAKE_CXX_EXTENSIONS OFF)
 endif()
@@ -187,7 +187,7 @@ set(protobuf_LINK_LIBATOMIC false)
 if (NOT MSVC)
   include(CheckCXXSourceCompiles)
   set(OLD_CMAKE_REQUIRED_FLAGS ${CMAKE_REQUIRED_FLAGS})
-  set(CMAKE_REQUIRED_FLAGS ${CMAKE_REQUIRED_FLAGS} -std=c++11)
+  set(CMAKE_REQUIRED_FLAGS ${CMAKE_REQUIRED_FLAGS} -std=c++14)
   check_cxx_source_compiles("
     #include <atomic>
     int main() {
diff --git a/WORKSPACE b/WORKSPACE
index ebe53315f03..20e7415a629 100644
--- a/WORKSPACE
+++ b/WORKSPACE
@@ -9,11 +9,10 @@ local_repository(
 
 http_archive(
     name = "com_google_googletest",
-    sha256 = "9dc9157a9a1551ec7a7e43daea9a694a0bb5fb8bec81235d8a1e6ef64c716dcb",
-    strip_prefix = "googletest-release-1.10.0",
+    sha256 = "ea54c9845568cb31c03f2eddc7a40f7f83912d04ab977ff50ec33278119548dd",
+    strip_prefix = "googletest-4c9a3bb62bf3ba1f1010bf96f9c8ed767b363774",
     urls = [
-        "https://mirror.bazel.build/github.com/google/googletest/archive/release-1.10.0.tar.gz",
-        "https://github.com/google/googletest/archive/release-1.10.0.tar.gz",
+        "https://github.com/google/googletest/archive/4c9a3bb62bf3ba1f1010bf96f9c8ed767b363774.tar.gz",
     ],
 )
 
diff --git a/examples/Makefile b/examples/Makefile
index 2a64b64fe37..ef7a4ef58e9 100644
--- a/examples/Makefile
+++ b/examples/Makefile
@@ -41,11 +41,11 @@ protoc_middleman_dart: addressbook.proto
 
 add_person_cpp: add_person.cc protoc_middleman
 	pkg-config --cflags protobuf  # fails if protobuf is not installed
-	c++ -std=c++11 add_person.cc addressbook.pb.cc -o add_person_cpp `pkg-config --cflags --libs protobuf`
+	c++ -std=c++14 add_person.cc addressbook.pb.cc -o add_person_cpp `pkg-config --cflags --libs protobuf`
 
 list_people_cpp: list_people.cc protoc_middleman
 	pkg-config --cflags protobuf  # fails if protobuf is not installed
-	c++ -std=c++11 list_people.cc addressbook.pb.cc -o list_people_cpp `pkg-config --cflags --libs protobuf`
+	c++ -std=c++14 list_people.cc addressbook.pb.cc -o list_people_cpp `pkg-config --cflags --libs protobuf`
 
 add_person_dart: add_person.dart protoc_middleman_dart
 
diff --git a/python/setup.py b/python/setup.py
index 283c99af607..3364ba9f7ad 100755
--- a/python/setup.py
+++ b/python/setup.py
@@ -331,7 +331,7 @@ def HasLibraryDirsOpt():
       extra_compile_args.append('-Wno-invalid-offsetof')
       extra_compile_args.append('-Wno-sign-compare')
       extra_compile_args.append('-Wno-unused-variable')
-      extra_compile_args.append('-std=c++11')
+      extra_compile_args.append('-std=c++14')
 
     if sys.platform == 'darwin':
       extra_compile_args.append('-Wno-shorten-64-to-32')
