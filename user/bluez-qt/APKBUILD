# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=bluez-qt
pkgver=5.94.0
pkgrel=0
pkgdesc="Qt integration with BlueZ"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires running D-Bus server.
license="LGPL-2.1-only OR LGPL-3.0-only"
depends=""
makedepends="cmake doxygen extra-cmake-modules qt5-qtbase-dev
	qt5-qtdeclarative-dev qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/bluez-qt-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		-DBUILD_TESTING:BOOL=OFF \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="9599376056de2711faba6e74f6259caa5d641f4c1dadd84967f184736d0437195d0789c322fd81dc3767bafc99232d0899e7728ba90a8c6bac7b8f482fad0ac9  bluez-qt-5.94.0.tar.xz"
