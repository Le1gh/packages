# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=lxqt-sudo
pkgver=1.4.0
_lxqt=0.13.0
pkgrel=0
pkgdesc="Graphical LXQt utility for sudo/su"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends="sudo"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev lxqt-build-tools>=$_lxqt
	liblxqt-dev>=${pkgver%.*} qt5-qttools-dev kwindowsystem-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxqt/lxqt-sudo/releases/download/$pkgver/lxqt-sudo-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="0f9eda1ae4272410b18fdce1059702715c1f8ea3d75893a00d18890b713cd0269a727f95e95959a3156d998447770b666119b068c2d1af9686938fb03a59e16e  lxqt-sudo-1.4.0.tar.xz"
