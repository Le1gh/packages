# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=py3-pycairo
pkgver=1.26.0
pkgrel=0
pkgdesc="Python bindings for Cairo"
url="https://pycairo.readthedocs.io/"
arch="all"
options="!check"  # Test suite requires unpackaged py3-flake8.
license="MIT AND (LGPL-2.1-only OR MPL-1.1)"
depends=""
checkdepends="py3-pytest"
makedepends="python3-dev cairo-dev meson"
subpackages="$pkgname-dev"
source="https://files.pythonhosted.org/packages/source/p/pycairo/pycairo-$pkgver.tar.gz"
builddir="$srcdir/pycairo-$pkgver"

build() {
	meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--buildtype=release \
		. output
	ninja -C output
}

check() {
	ninja -C output test
}

package() {
	DESTDIR="$pkgdir" ninja -C output install
}

sha512sums="5313f0d408a6e6ce6e70ac92291be5dd8651e01fbf9411d0467061afa21849cde27db273f2a13b4c3c931183f63f75f31fc0f0d3283b8f339ba88e71eab432f1  pycairo-1.26.0.tar.gz"
