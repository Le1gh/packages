# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcolorchooser
pkgver=22.04.2
pkgrel=0
pkgdesc="Simple application to choose a colour from the screen"
url="https://www.kde.org/applications/graphics/kcolorchooser/"
arch="all"
license="MIT"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev ki18n-dev kxmlgui-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcolorchooser-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="611a841fd971f24dd461d13db5ab7d7bec7c12494f07056e87890c6a5743238e8ef55551e8f22b9b5ae54dcb98ae93f71c5e611fab69cf032c40d6c5bf9c8e99  kcolorchooser-22.04.2.tar.xz"
