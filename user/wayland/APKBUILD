# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Bartłomiej Piotrowski <bpiotrowski@alpinelinux.org>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=wayland
pkgver=1.23.1
pkgrel=0
pkgdesc="A computer display server protocol"
url="https://wayland.freedesktop.org"
arch="all"
license="MIT"
depends=""
depends_dev="expat-dev"
makedepends="$depends_dev doxygen xmlto graphviz grep libffi-dev libxml2-dev
	bash meson"
subpackages="$pkgname-dev"
replaces="wayland-libs-client wayland-libs-cursor wayland-libs-server"
source="https://gitlab.freedesktop.org/wayland/wayland/-/releases/$pkgver/downloads/$pkgname-$pkgver.tar.xz"

build() {
	meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--buildtype=plain \
		-Ddocumentation=false \
		build

	ninja -C build
}

check() {
	export XDG_RUNTIME_DIR=$(mktemp -d)
	ninja -C build test
	rm -fr "${XDG_RUNTIME_DIR}"
}

package() {
	DESTDIR="$pkgdir" ninja -C build install
}

dev() {
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$pkgdir"/usr/share \
		"$subpkgdir"/usr
	default_dev
}

sha512sums="818eda003e3f7aa15690eedb1ff227a6056b2ce54bf23d45ffe573dc40a914623c5a1358218b59444dcdc483db0503324f0d27091d0ea954412a8b290de5f50a  wayland-1.23.1.tar.xz"
