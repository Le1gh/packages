# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kwidgetsaddons
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework containing ready-made widgets for common tasks"
url="https://www.kde.org/"
arch="all"
options="!check"  # Test suite requires running X11 server (passes on ciall)
# Tests are GPL-2.0+, but not shipped.
license="LGPL-2.1+ AND Unicode-DFS-2016"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen
	graphviz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwidgetsaddons-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="321c742465d911fe7358e77aef6f73175387eb906b9247a2e81880efb5c4d335bb2045186eb73a6425869d1372792785cd1165077eaa7414acb10384ea45ee4d  kwidgetsaddons-5.94.0.tar.xz"
