# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=mistserver
pkgver=3.1
pkgrel=0
pkgdesc="Multimedia streaming services"
url="https://mistserver.org"
arch="all"
options=""
license="Zlib AND AGPL-3.0-only"
depends=""
install="$pkgname.pre-install"
makedepends="cmake libexecinfo-dev mbedtls-dev libsrtp-dev"
pkgusers="mistserver"
pkggroups="mistserver"
subpackages="$pkgname-dev $pkgname-openrc"
# ATTENTION MAINTAINERS: Generate a new aggregate patch from:
# (assuming rebased on upstream correctly)
# git clone https://github.com/gizahNL/mistserver.git
# cd mistserver
# git format-patch -M origin/master --stdout > gizahnl-mbedtls-dev-from-origin-master.patch
source="mistserver-$pkgver.tar.gz::https://github.com/DDVTECH/mistserver/archive/refs/tags/$pkgver.tar.gz
	mistserver.confd
	mistserver.initd
	link-execinfo.patch

	add-dtls-srtp-cmake-option.patch
	gizahnl-mbedtls-dev-from-origin-master.patch
	fix-cmake-test-format.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS -fPIC" \
		-DCMAKE_C_FLAGS="$CFLAGS -fPIC" \
		-DUSE_MBEDTLS_SSL_DTLS_SRTP=True \
		${CMAKE_CROSSOPTS} \
		.
	make -j1  # do not increase this (race conditions)
}

check() {
	# FIXME!!!
	#CTEST_OUTPUT_ON_FAILURE=TRUE ctest

	# temporary sanity check
	./MistSession -v | grep Built
}

package() {
	make DESTDIR="$pkgdir" install

	# OpenRC

        install -Dm755 "$srcdir"/mistserver.initd "$pkgdir"/etc/init.d/mistserver
        install -Dm644 "$srcdir"/mistserver.confd "$pkgdir"/etc/conf.d/mistserver
}

sha512sums="efcac86cf031c5cc13dd274a4d63292122f1ef3d46faea0457e075898cda01bdea29f011699b595e07c8ed984886a33da2a04395a67698d6b2b405325f1b9715  mistserver-3.1.tar.gz
7288adab6589f2facc1cb794057b1c5d9ec94e12e60d6afc8f6f25c54a8e908cc9841b83b5a6e608fa799fd6aa11767e92a963004182d45f7be9ccd3b65097e7  mistserver.confd
e0c7df42f4d486983ece1ea50ab8f3006ebab5386881c14c4b2ff1246b6dd38ace935dc54f8f8a7687edb7ca5975b8c26abd6e99957b8c892862732263d49eb9  mistserver.initd
a27bac965078f7eafb339ae7be9e50519d5728ae4f5d725905d5eecbb3fdf048df3e150cfa881be4bab754ca674a11271343156d5d97758d2ca65bef5bff55a6  link-execinfo.patch
f90737722ac4a2ecff64386a9287ce0ddd48e7b176239f3de26725cadace52667ab44febe536738d8e0dba1fee2047e8f65caa8a2f282c7c6e9dbcc4f8daa23a  add-dtls-srtp-cmake-option.patch
48e835a09b8096f78e94f25429768debf65ab9f4b3152c45b69eb072ac5d3abc0036daae894e6b85c57ad7ae7993bf2940c730fbb02dcafecd2ed9716b86dfb9  gizahnl-mbedtls-dev-from-origin-master.patch
de8bc5279426c7fab58074a4e73001c590dd386c0d7cd1747e6fff74278aa901662e7720bf97ea645eb28fc1a6c3e4830afdcaf5e4a770c996c639936dd3e28b  fix-cmake-test-format.patch"
