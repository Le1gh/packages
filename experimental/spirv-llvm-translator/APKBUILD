# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=spirv-llvm-translator
pkgver=18.1.6
_llvmver=$(echo $pkgver | cut -f1 -d.)
pkgrel=0
pkgdesc="A tool and a library for bi-directional translation between SPIR-V and LLVM IR"
url=" "
arch="all"
license="NCSA"
depends=""
makedepends="cmake llvm${_llvmver}-dev spirv-headers spirv-tools-dev"
subpackages="$pkgname-dev"
source="SPIRV-LLVM-Translator-$pkgver.tar.gz::https://github.com/KhronosGroup/SPIRV-LLVM-Translator/archive/refs/tags/v$pkgver.tar.gz
	endian.patch
	test-triplet.patch
	"
builddir="$srcdir/SPIRV-LLVM-Translator-$pkgver"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCCACHE_ALLOWED=OFF \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DLLVM_DIR=$(/usr/lib/llvm${_llvmver}/bin/llvm-config --cmakedir) \
		-DLLVM_EXTERNAL_LIT=/usr/bin/lit \
		-DLLVM_EXTERNAL_SPIRV_HEADERS_SOURCE_DIR=/usr/include/spirv \
		-DLLVM_SPIRV_INCLUDE_TESTS=ON \
		${CMAKE_CROSSOPTS} \
		-Bbuild .
	make -C build
}

check() {
	make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="30d85fcd767cdae8b29f65dff50f2449e3421477634edcec67e88e92eeb77ec724c46eed4e90274d8697955c79fc26650c268839ed4612aee096b2d4707af728  SPIRV-LLVM-Translator-18.1.6.tar.gz
fd261c9bb17c1f322191375ebbbbf14ebb8c6ec36d777470e48a5856d936c63f7a3306784460ee903b18f97b34f5157a2590bcfd3c891ebe03ce865f7d5ee9f7  endian.patch
45d38dcf180113e2790b9a1fa3d1f7b24975b67e6976af71688bd6cbbd23e8e1cc792593f4612277bf6fee45e41a3047c62e3a69a218ee246f1494c9e1783665  test-triplet.patch"
