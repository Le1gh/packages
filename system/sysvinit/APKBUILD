# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=sysvinit
pkgver=3.11
pkgrel=0
pkgdesc="System V-style init programs"
url="https://savannah.nongnu.org/projects/sysvinit"
arch="all"
license="GPL-2.0+"
options="!check"
depends="s6-linux-init-common"
makedepends="linux-headers utmps-dev"
install="sysvinit.post-upgrade sysvinit.post-install"
provides="/sbin/init=0"
subpackages="$pkgname-doc"
source="https://github.com/slicer69/sysvinit/releases/download/$pkgver/$pkgname-$pkgver.tar.xz
	inittab-2.88
	utmpx.patch
	s6-svscanboot
	"

prepare() {
	default_prepare

	# util-linux
	sed -i -r \
		-e '/^(USR)?S?BIN/s:\<(last|lastb|mesg)\>::g' \
		-e '/^MAN[18]/s:\<(last|lastb|mesg)[.][18]\>::g' \
		src/Makefile

	# broken
	sed -i -r \
		-e '/^USRBIN/s:utmpdump::g' \
		-e '/^MAN1/s:utmpdump\.1::g' \
		src/Makefile

	# procps
	sed -i -r \
		-e '/\/bin\/pidof/d'\
		-e '/^MAN8/s:\<pidof.8\>::g' \
		src/Makefile

	# e2fsprogs
	sed -i -r \
		-e '/^(USR)?S?BIN/s:\<logsave\>::g' \
		-e '/^MAN8/s:\<logsave\.8\>::g' \
		src/Makefile
}

build() {
	export DISTRO="Adélie"
	make -C src
}

_install_s6_stuff() {
	svcimg="$pkgdir/etc/s6-linux-init/current/run-image/service"
	mkdir -p -m 0755 "$pkgdir/sbin" "$svcimg/.s6-svscan" "$svcimg/s6-svscan-log"
	{ echo '#!/bin/execlineb -P' ; echo 'false' ; } > "$svcimg/.s6-svscan/crash"
	chmod 0755 "$svcimg/.s6-svscan/crash"
	{ echo '#!/bin/execlineb -P' ; echo 's6-svc -x -- /run/service/s6-svscan-log' ; } > "$svcimg/.s6-svscan/finish"
	chmod 0755 "$svcimg/.s6-svscan/finish"
	{ echo '#!/bin/execlineb -P' ; echo 'redirfd -rnb 0 fifo' ; echo 's6-setuidgid catchlog' ; echo 's6-log -bd3 -- t /run/uncaught-logs' ; } > "$svcimg/s6-svscan-log/run"
	chmod 0755 "$svcimg/s6-svscan-log/run"
	install -D -m 0755 "$srcdir"/s6-svscanboot "$pkgdir/sbin/s6-svscanboot"
}

package() {
	make -C src install ROOT="$pkgdir"
	rm "$pkgdir"/usr/bin/lastb || true
	install -D -m644 "$srcdir"/inittab-2.88 "$pkgdir"/etc/inittab
	_install_s6_stuff
}

sha512sums="12e2d11b76702b493e8f083e5409b98a1daf41a8d9fb7ef8a36416bb0310d5a26b92eaee0c9396c03cf08842258b953f79541ae147ef730f3bc54530da4d1029  sysvinit-3.11.tar.xz
87668b49690091a227c0384fd2400f1006d24c27cc27a25efa7eba56839ccb1eead00b58ce4b654eab9c0208d68aa2cbb888fd5f2990905845aa9688442a69a0  inittab-2.88
78d04e33099de13b40243ac0be3e93bf4f2addcee3155c799e711ffec0dc003bf416d956d302aba92ec3e80d2dc6b2d73da0133e3466fce49531f672190ca2d9  utmpx.patch
e52fd49daa5abfc583f1973f3428b1e00a71e7136a8bc6418e94b345d53ef250b3b3c3bee389fe37872b26a78d0957ae852e221428f33b2c728dfd3d50b59634  s6-svscanboot"
