# Contributor: Sergey Lukin <sergej.lukin@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=icu
pkgver=75.1

# convert x.y.z to x_y_z
_ver=$(printf '%s' "$pkgver" | tr . _)

pkgrel=0
pkgdesc="International Components for Unicode"
url="https://icu.unicode.org"
arch="all"
license="ICU"
depends=""
checkdepends="diffutils python3"
makedepends=""
subpackages="$pkgname-static $pkgname-dev $pkgname-doc $pkgname-libs"
source="https://github.com/unicode-org/icu/releases/download/release-$(printf '%s' "$pkgver" | tr . -)/${pkgname}4c-$_ver-src.tgz
	icu-60.2-always-use-utf8.patch
	islamic-calendar.patch
	"

# secfixes:
#   57.1-r1:
#     - CVE-2016-6293
#   58.1-r1:
#     - CVE-2016-7415
#   58.2-r2:
#     - CVE-2017-7867
#     - CVE-2017-7868
#   65.1-r1:
#     - CVE-2020-10531

builddir="$srcdir"/icu/source

prepare() {
	default_prepare
	update_config_sub

	# https://unicode-org.atlassian.net/browse/ICU-6102
	for x in ARFLAGS CFLAGS CPPFLAGS CXXFLAGS FFLAGS LDFLAGS; do
		sed -i -e "/^${x} =.*/s:@${x}@::" "config/Makefile.inc.in"
	done
}

build() {
	# GCC 13 changed default fp precision behavior. (#1193)
	export CXXFLAGS="${CXXFLAGS} -fexcess-precision=fast"

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--enable-shared \
		--enable-static \
		--with-data-packaging=library \
		--disable-samples \
		--mandir=/usr/share/man
	# Weird build system glitch.
	mkdir data/out
	make
}

check() {
	TZ=UTC make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
	chmod +x "$pkgdir"/usr/bin/icu-config
	install -Dm644 "$srcdir"/icu/license.html \
		"$pkgdir"/usr/share/licenses/icu/license.html
}

static() {
	pkgdesc="$pkgdesc (Static libraries)"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/*.a "$subpkgdir"/usr/lib/
}

sha512sums="70ea842f0d5f1f6c6b65696ac71d96848c4873f4d794bebc40fd87af2ad4ef064c61a786bf7bc430ce4713ec6deabb8cc1a8cc0212eab148cee2d498a3683e45  icu4c-75_1-src.tgz
f89e9f389621ed9a58548ac961fc93fca8377b91fbb0df1991049757deadf72f6f11346eb7af883040c85b28621140805636a7f90d94f3b53e666d67b9681989  icu-60.2-always-use-utf8.patch
1a4305e6145ecab171d044e4cc14e074294c73e221f780803363823bafa2f79d2efd77d63f6c0bb6df6d5f9767de38d321196d85649be929a583f1abc27dbdd0  islamic-calendar.patch"
