# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Síle Ekaterin Liszka <sheila@adelielinux.org> 
pkgname=nghttp2
pkgver=1.52.0
pkgrel=0
pkgdesc="Experimental HTTP/2 client, server and proxy"
url="https://nghttp2.org/"
arch="all"
options="!check"  # Requires cunit, which we don't currently ship
license="MIT"
depends=""
makedepends=""
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs"
source="https://github.com/tatsuhiro-t/$pkgname/releases/download/v$pkgver/nghttp2-$pkgver.tar.xz"

# secfixes:
#   1.41.0-r0:
#     - CVE-2020-11080

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-static \
		--without-neverbleed \
		--without-jemalloc \
		--enable-lib-only
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="3af1ce13270f7afc8652bd3de71200d9632204617fe04d2be7156d60eeb1a5cc415573677791a399ae03577e8e3256939b1b05d27dbd98dee504d09ec5325d56  nghttp2-1.52.0.tar.xz"
