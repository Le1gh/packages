# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=bubblewrap
pkgver=0.6.1
pkgrel=0
pkgdesc="Unprivileged sandboxing tool"
url="https://github.com/projectatomic/bubblewrap"
arch="all"
options="!check suid"  # requires suid to already be set in order to check
license="LGPL-2.0+"
makedepends="autoconf automake libcap-dev docbook-xsl"
checkdepends="python3 sudo"
subpackages="$pkgname-nosuid $pkgname-doc
	$pkgname-bash-completion:bashcomp:noarch"
source="bubblewrap-$pkgver.tar.gz::https://github.com/containers/bubblewrap/archive/v$pkgver.tar.gz"

# secfixes:
#   0.3.3-r0:
#     - CVE-2019-12439
#   0.4.1-r0:
#     - GHSA-j2qp-rvxj-43vj

prepare() {
	default_prepare
	NOCONFIGURE=1 ./autogen.sh
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--with-priv-mode=setuid
	make
}

check() {
	# 1. chown root and chmod u+s $builddir/test-bwrap
	# 2. Run abuild check (suid test)
	# 3. Unset permissions on test-bwrap
	# 4. Run abuild check again (nosuid test)
	#
	# As of 0.4.1, all tests pass except those relating to bind mounts
	# over symlinks. Those tests fail because musl's realpath depends on
	# the availability of /proc, which is not available in the middle of
	# the setup procedure since pivot_root has been performed at least
	# once. They have been patched to be skipped.
	make check
}

package() {
	make install DESTDIR="$pkgdir"
}

nosuid() {
	pkgdesc="$pkgdesc (non-setuid binary)"

	mkdir -p "$subpkgdir"/usr/bin
	cp "$pkgdir"/usr/bin/bwrap \
		"$subpkgdir"/usr/bin/bwrap.nosuid
	chmod -s "$subpkgdir"/usr/bin/bwrap.nosuid
}

bashcomp() {
	pkgdesc="Bash completions for $pkgname"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	mkdir -p "$subpkgdir"/usr/share/
	mv "$pkgdir"/usr/share/bash-completion/ "$subpkgdir"/usr/share/
}

sha512sums="30743cadc4f536430e253e1e21af5e0e56ae5dc4a11258faa42321b04b7490b54f904e72fc0147b23b0a0524870f41b5805c1482d20a11e0c4549bf8ae1d651f  bubblewrap-0.6.1.tar.gz"
