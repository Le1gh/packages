# Contributor: Ariadne Conill <ariadne@dereferenced.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libssh2
pkgver=1.10.0
pkgrel=0
pkgdesc="Library for accessing SSH servers"
url="https://libssh2.org/"
arch="all"
license="BSD-3-Clause"
checkdepends="openssh-server"
makedepends="openssl-dev zlib-dev"
subpackages="$pkgname-dev $pkgname-doc"
#< zv> heads up that ipv6 on www.libssh2.org seems to be down
#< bagder> yeah, I know. its a misconfigure thing since a while back
#source="https://www.libssh2.org/download/libssh2-$pkgver.tar.gz
source="https://github.com/libssh2/libssh2/releases/download/$pkgname-$pkgver/$pkgname-$pkgver.tar.gz
	test-sshd.patch
	fix-tests.patch
	"

# secfixes:
#   1.9.0-r1:
#     - CVE-2019-17498

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-rpath
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="e064ee1089eb8e6cd5fa2617f4fd8ff56c2721c5476775a98bdb68c6c4ee4d05c706c3bb0eb479a27a8ec0b17a8a5ef43e1d028ad3f134519aa582d3981a3a30  libssh2-1.10.0.tar.gz
d6d4dfcd96bfa08210ecfb94c71624e011f294bab0b88548f41caff48283c2102ff012a6a6e71a100aa1d6430c8470fc0175449f31ae4583b6f8cfa224983603  test-sshd.patch
d075765062e3ffefb90902c9644ecde17f9bb39eac8ab5759370a26206f5804cb71b8b2d9b8d3ce9989c209bc36483dbc25f894f8c0ec9081aaef1a489c048df  fix-tests.patch"
