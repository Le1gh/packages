# Contributor: Ariadne Conill <ariadne@dereferenced.org>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=shadow
pkgver=4.13
pkgrel=0
pkgdesc="Login and password management utilities"
url="https://github.com/shadow-maint/shadow"
arch="all"
options="suid"
license="BSD-3-Clause OR Artistic-1.0-Perl"
depends=""
makedepends_build="docbook-xml docbook-xsl itstool libxslt"
makedepends_host="linux-pam-dev utmps-dev"
subpackages="$pkgname-doc $pkgname-dbg $pkgname-lang $pkgname-uidmap"
source="https://github.com/shadow-maint/shadow/releases/download/$pkgver/shadow-$pkgver.tar.xz
	login.pamd
	useradd-defaults.patch
	pam-useradd.patch
	"
# secfixes:
#   4.5-r0:
#   - CVE-2017-12424
#   4.2.1-r11:
#   - CVE-2017-2616
#   4.2.1-r7:
#   - CVE-2016-6252

build() {
	LIBS="-lutmps -lskarnet" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--target=$CTARGET \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-utmpx \
		--with-libpam \
		--without-audit \
		--without-selinux \
		--without-acl \
		--without-attr \
		--without-tcb \
		--without-nscd \
		--without-su \
		--without-group-name-max-length \
		--enable-man
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	# Do not install these pam.d files they are broken and outdated.
	rm "$pkgdir"/etc/pam.d/*

	# However, install our own for login.
	cp "$srcdir"/login.pamd "$pkgdir"/etc/pam.d/login

	# install some pam.d files based on a patched useradd
	for pamf in groupadd groupdel groupmems groupmod \
		 useradd userdel usermod
	do
		install -m0644 etc/pam.d/useradd \
			"$pkgdir/etc/pam.d/$pamf"
	done

	# nologin is provided by util-linux.
	rm "$pkgdir"/sbin/nologin

	# /etc/login.defs is not very useful - replace it with an *almost* blank file.
	rm "$pkgdir"/etc/login.defs
	echo "USERGROUPS_ENAB yes" > "$pkgdir"/etc/login.defs

	# Avoid conflict with man-pages.
	rm "$pkgdir"/usr/share/man/man3/getspnam.3* \
		"$pkgdir"/usr/share/man/man5/passwd.5*
	# and util-linux-doc
	rm "$pkgdir"/usr/share/man/man8/nologin.8*
}

uidmap() {
	pkgdesc="Utilities for using subordinate UIDs and GIDs"

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/new*idmap "$subpkgdir"/usr/bin/
	chmod 4711 "$subpkgdir"/usr/bin/new*idmap

	# Used e.g. for unprivileged LXC containers.
	mkdir "$subpkgdir"/etc
	touch "$subpkgdir"/etc/subuid "$subpkgdir"/etc/subgid
}

sha512sums="2949a728c3312bef13d23138d6b79caf402781b1cb179e33b5be546c1790971ec20778d0e9cd3dbe09691d928ffcbe88e60da42fab58c69a90d5ebe5e3e2ab8e  shadow-4.13.tar.xz
46a6f83f3698e101b58b8682852da749619412f75dfa85cecad03d0847f6c3dc452d984510db7094220e4570a0565b83b0556e16198ad894a3ec84b3e513d58d  login.pamd
fb6b19b05a58f5200d4ad39c7393735ef8e0db853c50ba2ad29efb7bdd871bc8659f6845c9cc0660ce12372dbc78b7df9cfd8a06f6913036d905e5372f2ed99f  useradd-defaults.patch
0b4587e263cb6be12fa5ae6bc3b3fc4d3696dae355bc67d085dc58c52ff96edb4d163b95db2092b8c2f3310839430cac03c7af356641b42e24ee4aa6410f5cf1  pam-useradd.patch"
