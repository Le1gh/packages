# Contributor: Sergey Lukin <sergej.lukin@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=pcre
pkgver=8.45
pkgrel=0
pkgdesc="Perl-compatible regular expression library"
url="https://pcre.org"
arch="all"
license="BSD-3-Clause"
depends=""
makedepends=""
subpackages="$pkgname-dev $pkgname-doc $pkgname-tools
	libpcrecpp libpcre16 libpcre32"
source="$pkgname-$pkgver.tar.bz2::https://sourceforge.net/projects/pcre/files/pcre/$pkgver/$pkgname-$pkgver.tar.bz2/download
	stack-frame-size-detection.patch
	"

# secfixes:
#   8.40-r2:
#   - CVE-2017-7186
#   7.8-r0:
#   - CVE-2017-11164

build() {
	_enable_jit="--enable-jit"
	[ "$CARCH" = "s390x" ] && _enable_jit=""
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--libdir=/lib \
		$_enable_jit \
		--enable-utf8 \
		--enable-unicode-properties \
		--enable-pcre8 \
		--enable-pcre16 \
		--enable-pcre32 \
		--with-match-limit-recursion=8192 \
		--htmldir=/usr/share/doc/$pkgname-$pkgver/html \
		--docdir=/usr/share/doc/$pkgname-$pkgver

	make
}

package() {
	make DESTDIR="$pkgdir" install
	mkdir -p "$pkgdir"/usr/lib
	mv "$pkgdir"/lib/pkgconfig "$pkgdir"/usr/lib/
}

check() {
	# skip locale specific tests
	sed -i -e 's/do3=yes//g' RunTest

	make check
}

libpcrecpp() {
	pkgdesc="C++ bindings for PCRE"
	mkdir -p "$subpkgdir"/lib
	mv "$pkgdir"/lib/libpcrecpp.so* "$subpkgdir"/lib/
}

libpcre16() {
	pkgdesc="PCRE with 16 bit character support"
	mkdir -p "$subpkgdir"/lib
	mv "$pkgdir"/lib/libpcre16.so* "$subpkgdir"/lib/
}

libpcre32() {
	pkgdesc="PCRE with 32 bit character support"
	mkdir -p "$subpkgdir"/lib
	mv "$pkgdir"/lib/libpcre32.so* "$subpkgdir"/lib/
}

tools() {
	pkgdesc="Auxiliary utilities for PCRE"
	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="91bff52eed4a2dfc3f3bfdc9c672b88e7e2ffcf3c4b121540af8a4ae8c1ce05178430aa6b8000658b9bb7b4252239357250890e20ceb84b79cdfcde05154061a  pcre-8.45.tar.bz2
a088304318f7a04f90263efbab20b423e67fb6e3d829e7bc080fd44c29e98bc29cab992b73ed1b992af89cdf4606667af20572558a3a96549b7bd9479d0e6d88  stack-frame-size-detection.patch"
