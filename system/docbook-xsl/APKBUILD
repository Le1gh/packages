# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=docbook-xsl
pkgver=1.79.1
pkgrel=4
pkgdesc="XML stylesheets for DocBook transformations"
url="http://docbook.sourceforge.net/"
arch="noarch"
options="!check"  # Just XML files
license="MIT"
depends="libxml2-utils libxslt docbook-xml"
subpackages="$pkgname-ns"
makedepends=""
install="$pkgname.post-install $pkgname.post-upgrade $pkgname.post-deinstall
	$pkgname-ns.post-install $pkgname-ns.post-upgrade
	$pkgname-ns.post-deinstall"
source="http://downloads.sourceforge.net/sourceforge/docbook/$pkgname-$pkgver.tar.bz2
	https://downloads.sourceforge.net/project/docbook/docbook-xsl-ns/$pkgver/$pkgname-ns-$pkgver.tar.bz2
	765567_non-recursive_string_subst.patch
	"

package() {
	local _dest dir f
	_dest="$pkgdir"/usr/share/xml/docbook/xsl-stylesheets

	install -dm755 "$_dest"
	install -m644 VERSION VERSION.xsl "$_dest"/

	for dir in assembly common eclipse epub epub3 fo highlighting html \
		htmlhelp javahelp lib manpages params profiling roundtrip \
		template website xhtml xhtml-1_1 xhtml5; do

		install -dm755 $_dest/$dir
		for f in $dir/*.xml $dir/*.xsl $dir/*.dtd $dir/*.ent; do
			[ -e "$f" ] || continue
			install -m644 $f $_dest/$dir
		done
	done

	install -dm755 "$pkgdir"/etc/xml

	install -m644 -D COPYING \
		"$pkgdir"/usr/share/licenses/$pkgname/COPYING
}

ns() {
	cd "$srcdir/${pkgname}-ns-${pkgver}"
	local _dest dir f
	_dest="$subpkgdir"/usr/share/xml/docbook/xsl-stylesheets-ns

	install -dm755 "$_dest"
	install -m644 VERSION VERSION.xsl "$_dest"/

	for dir in assembly common eclipse epub epub3 fo highlighting html \
		htmlhelp javahelp lib manpages params profiling roundtrip \
		template website xhtml xhtml-1_1 xhtml5; do

		install -dm755 $_dest/$dir
		for f in $dir/*.xml $dir/*.xsl $dir/*.dtd $dir/*.ent; do
			[ -e "$f" ] || continue
			install -m644 $f $_dest/$dir
		done
	done

	install -dm755 "$subpkgdir"/etc/xml

	install -m644 -D COPYING \
		"$subpkgdir"/usr/share/licenses/$pkgname/COPYING
}

sha512sums="83325cbaf1545da6b9b8b77f5f0e6fdece26e3c455164b300a1aa3d19e3bd29ae71fd563553a714a5394968d1a65684c6c7987c77524469358d18b8c227025c7  docbook-xsl-1.79.1.tar.bz2
23fd06870bd5afe4efcd08c8ad679821c202a62442b50657c093cbe9cd71b585a3c56a5534a0d41119f58bf98b1f9014a53ff2e48ab59075ec1827e7363980e9  docbook-xsl-ns-1.79.1.tar.bz2
6a26838078a3ce28273dddfa1af6a378cffc28b6d1ba48a4cfc839addd7bf58ce217d6584b735f9c75381954744ab2386c75fa3c593858b6e27882be55c00d04  765567_non-recursive_string_subst.patch"
