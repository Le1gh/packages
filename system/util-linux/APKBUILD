# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=util-linux
pkgver=2.38.1

case $pkgver in
	*.*.*) _v=${pkgver%.*};;
	*.*) _v=$pkgver;;
esac

pkgrel=2
pkgdesc="Official Linux system management utilities"
url="https://git.kernel.org/pub/scm/utils/util-linux/util-linux.git"
arch="all"
options="!check suid"  # multibyte input fails because of musl locale stuff
license="GPL-2.0-only AND GPL-2.0+ AND LGPL-2.1+ AND Public-Domain"
depends=""
makedepends_build="autoconf automake libtool"
makedepends_host="zlib-dev ncurses-dev linux-headers linux-pam-dev libcap-ng-dev
	utmps-dev"
subpackages="$pkgname-doc $pkgname-dev $pkgname-bash-completion:bashcomp:noarch
	libuuid $pkgname-lang"
makedepends="$makedepends_build $makedepends_host"
replaces="e2fsprogs util-linux-ng sfdisk cfdisk findmnt mcookie blkid setpriv
	libblkid libsmartcols libmount libfdisk shadow"
provides="sfdisk=$pkgver-r$pkgrel cfdisk=$pkgver-r$pkgrel
	findmnt=$pkgver-r$pkgrel mcookie=$pkgver-r$pkgrel
	blkid=$pkgver-r$pkgrel libblkid=$pkgver-r$pkgrel
	libmount=$pkgver-r$pkgrel libsmartcols=$pkgver-r$pkgrel
	libfdisk=$pkgver-r$pkgrel"
source="https://www.kernel.org/pub/linux/utils/$pkgname/v${_v}/$pkgname-$pkgver.tar.xz
	ttydefaults.h
	0000-utmps-paths.patch
	"

prepare() {
	default_prepare

	cp "$srcdir"/ttydefaults.h include/
	libtoolize --force && aclocal -I m4 && autoconf \
		&& automake --add-missing
}

build() {
	# login utils are provided by shadow
	LIBS="-lutmps -lskarnet" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--with-sysroot=$CBUILDROOT \
		--prefix=/usr \
		--enable-su \
		--disable-raw \
		--disable-uuidd \
		--disable-tls \
		--disable-kill \
		--disable-login \
		--disable-chfn-chsh \
		--without-python
	make
}

package() {
	make -j1 DESTDIR="$pkgdir" install

	mkdir -p "$pkgdir"/etc/default
	printf "ALWAYS_SET_PATH yes\n" > "$pkgdir"/etc/default/su
}

dev() {
	default_dev
	replaces="e2fsprogs-dev util-linux-ng-dev"
}

bashcomp() {
	pkgdesc="Bash completions for $pkgname"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	mkdir -p "$subpkgdir"/usr/share/
	mv "$pkgdir"/usr/share/bash-completion \
		"$subpkgdir"/usr/share/
}

libuuid() {
	pkgdesc="DCE compatible Universally Unique Identifier library"
	license="BSD-3-Clause"
	depends=""
	mkdir -p "$subpkgdir"/lib
	mv "$pkgdir"/lib/libuuid* "$subpkgdir"/lib/
}

libmount() {
	pkgdesc="Block device identification library from util-linux"
	license="LGPL-2.1+"
	depends=""
	mkdir -p "$subpkgdir"/lib
	mv "$pkgdir"/lib/libmount.so.* "$subpkgdir"/lib/
}

sha512sums="07f11147f67dfc6c8bc766dfc83266054e6ede776feada0566b447d13276b6882ee85c6fe53e8d94a17c03332106fc0549deca3cf5f2e92dda554e9bc0551957  util-linux-2.38.1.tar.xz
876bb9041eca1b2cca1e9aac898f282db576f7860aba690a95c0ac629d7c5b2cdeccba504dda87ff55c2a10b67165985ce16ca41a0694a267507e1e0cafd46d9  ttydefaults.h
b1d992b58af516bd4c19dfa3f7df2680f4d0c31608fd20b5ae5eab23138df00666a8b1895d8d19d8afb66ce5f535f04a1ce81b248ae69b1f68c991d6549e6726  0000-utmps-paths.patch"
