# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gdb
pkgver=12.1
pkgrel=1
pkgdesc="The GNU Debugger"
url="https://www.sourceware.org/gdb/"
arch="all"
options="!check"  # thousands of test failures
license="GPL-3.0+"
depends=""
makedepends="ncurses-dev expat-dev texinfo python3-dev gmp-dev
	zlib-dev linux-headers perl"
subpackages="$pkgname-doc $pkgname-lang"
source="https://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.xz
	s390x-use-elf-gdb_fpregset_t.patch
	ppc-musl.patch
	ppc-ptregs.patch
	remove-extraneous-include.patch
	arm64.patch
	"

build() {
	local _config="
		--build=$CBUILD
		--host=$CHOST
		--prefix=/usr
		--target=$CTARGET
		--disable-werror
		--mandir=/usr/share/man
		--infodir=/usr/share/info"

	# avoid generation of mangled and non-mangled objects on ppc64
	[ "$CARCH" = ppc64le ] && _config="$_config --enable-build-with-cxx=no"

	./configure $_config
	(cd opcodes && ./configure $_config)
	make
}

package() {
	make DESTDIR="$pkgdir" install

	# resolve conflict with binutils-doc
	rm -f "$pkgdir"/usr/share/info/bfd.info
	rm -f "$pkgdir"/usr/share/info/dir
	rm -f "$pkgdir"/usr/share/info/ctf-spec.info

	# resolve conflict with binutils-lang
	rm -f "$pkgdir"/usr/share/locale/*/LC_MESSAGES/bfd.mo

	# those are provided by binutils
	rm -rf "$pkgdir"/usr/include
	rm -rf "$pkgdir"/usr/lib
}

sha512sums="425568d2e84672177d0fb87b1ad7daafdde097648d605e30cf0656970f66adc6a82ca2d83375ea4be583e9683a340e5bfdf5819668ddf66728200141ae50ff2d  gdb-12.1.tar.xz
986e68275f7692f39b5d8aedeb9f9e88f0e5bebb3f8c7f104933c525d35ca54fc90e18698c1e3e1009e8a188e5e70d6f252cb39e4c75e37db7bf479017e0da32  s390x-use-elf-gdb_fpregset_t.patch
04911f87904b62dd7662435f9182b20485afb29ddb3d6398a9d31fef13495f7b70639c77fdae3a40e2775e270d7cd40d0cfd7ddf832372b506808d33c8301e01  ppc-musl.patch
6de518389970f9749def91bff38c9ed283160066cb0c0b0b62fe191229089f1c040a35c819a934d85e011fd06047bfb603086760fe607a5367b48dec38cfc45a  ppc-ptregs.patch
3ff31774ba78c1208415289566b901debd815da8b53acefe4a0785e7b2bbcff39585a556d44ff2f7d8d639ebc047620b96e72573acae376d8f23aa98dd1fe286  remove-extraneous-include.patch
f7beecfcd9c642930dec36d7b02d5a875bcdf07b5fad82a4ef3443332c0f60706d5b48c6ae8bb68bdec0398d3c32ef35e2478dcfb1eb7806b2699cfa2df29e01  arm64.patch"
