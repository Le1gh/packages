# Maintainer: Samuel Holland <samuel@sholland.org>
pkgname=rust
pkgver=1.79.0
_bootver=1.78.0-r0
_llvmver=18
pkgrel=0
pkgdesc="The Rust Programming Language"
url="https://www.rust-lang.org"
arch="all"
options="!check"  # Failures on aarch64 and ppc64.
license="(Apache-2.0 OR MIT) AND (NCSA OR MIT) AND BSD-2-Clause AND BSD-3-Clause"
depends="$pkgname-std=$pkgver-r$pkgrel gcc musl-dev"
makedepends="
	curl-dev
	llvm$_llvmver-dev
	llvm$_llvmver-test-utils
	openssl-dev
	python3
	cargo-bootstrap=$_bootver
	rust-bootstrap=$_bootver
	rustfmt-bootstrap=$_bootver
	zlib-dev
	"
provides="$pkgname-bootstrap=$pkgver-r$pkgrel"
subpackages="
	$pkgname-dbg
	$pkgname-std
	$pkgname-analysis
	$pkgname-doc
	$pkgname-gdb::noarch
	$pkgname-lldb::noarch
	$pkgname-src::noarch
	cargo
	cargo-clippy:_cargo_clippy
	cargo-fmt:_cargo_fmt
	cargo-doc:_cargo_doc:noarch
	cargo-bash-completion:_cargo_bashcomp:noarch
	cargo-zsh-completion:_cargo_zshcomp:noarch
	rustfmt
	"
source="https://static.rust-lang.org/dist/rustc-$pkgver-src.tar.xz
	0001-Fix-LLVM-build.patch
	0002-Fix-linking-to-zlib-when-cross-compiling.patch
	0003-Fix-rustdoc-when-cross-compiling-on-musl.patch
	0004-Remove-musl_root-and-CRT-fallback-from-musl-targets.patch
	0005-Prefer-libgcc_eh-over-libunwind-for-musl.patch
	0006-Link-libssp_nonshared.a-on-all-musl-targets.patch
	0007-test-failed-doctest-output-Fix-normalization.patch
	0008-test-sysroot-crates-are-unstable-Fix-test-when-rpath.patch
	0009-Ignore-broken-and-non-applicable-tests.patch
	0010-Link-stage-2-tools-dynamically-to-libstd.patch
	0011-Move-debugger-scripts-to-usr-share-rust.patch
	0012-Add-foxkit-target-specs.patch
	powerpc-atomics.patch
	"
builddir="$srcdir/rustc-$pkgver-src"
_rlibdir="/usr/lib/rustlib/$CTARGET/lib"

build() {
	cat > config.toml <<- EOF
		[build]
		doc-stage = 2
		build-stage = 2
		test-stage = 2
		build = "$CBUILD"
		host = [ "$CHOST" ]
		target = [ "$CTARGET" ]
		cargo = "/usr/bin/cargo"
		rustc = "/usr/bin/rustc"
		rustfmt = "/usr/bin/rustfmt"
		docs = true
		compiler-docs = false
		submodules = false
		python = "python3"
		locked-deps = true
		vendor = true
		extended = true
		tools = [ "analysis", "cargo", "clippy", "rustfmt", "src" ]
		verbose = 1
		sanitizers = false
		profiler = false
		cargo-native-static = false
		[install]
		prefix = "/usr"
		[rust]
		optimize = true
		debug = false
		codegen-units = 1
		debuginfo-level = 1
		debuginfo-level-rustc = 0
		debuginfo-level-tests = 0
		backtrace = true
		incremental = false
		parallel-compiler = false
		channel = "stable"
		description = "Adelie ${pkgver}-r${pkgrel}"
		rpath = false
		verbose-tests = true
		optimize-tests = true
		codegen-tests = true
		dist-src = false
		lld = false
		use-lld = false
		llvm-tools = false
		backtrace-on-ice = true
		remap-debuginfo = false
		jemalloc = false
		llvm-libunwind = "no"
		new-symbol-mangling = true
		[target.$CTARGET]
		cc = "$CTARGET-gcc"
		cxx = "$CTARGET-g++"
		ar = "ar"
		ranlib = "ranlib"
		linker = "$CTARGET-gcc"
		llvm-config = "/usr/lib/llvm$_llvmver/bin/llvm-config"
		crt-static = false
		[dist]
		src-tarball = false
		compression-formats = [ "xz" ]
	EOF

	LLVM_LINK_SHARED=1 \
	RUST_BACKTRACE=1 \
	python3 x.py dist -j ${JOBS:-2}
}

check() {
	LLVM_LINK_SHARED=1 \
	python3 x.py test -j ${JOBS:-2} --no-doc --no-fail-fast || true
}

package() {
	cd "$builddir"/build/dist

	tar xf rust-$pkgver-$CTARGET.tar.xz
	rust-$pkgver-$CTARGET/install.sh \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--sysconfdir="$pkgdir"/etc \
		--disable-ldconfig
	tar xf rust-src-$pkgver.tar.xz
	rust-src-$pkgver/install.sh \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--disable-ldconfig

	rm "$pkgdir"/usr/lib/rustlib/components \
	   "$pkgdir"/usr/lib/rustlib/install.log \
	   "$pkgdir"/usr/lib/rustlib/manifest-* \
	   "$pkgdir"/usr/lib/rustlib/rust-installer-version \
	   "$pkgdir"/usr/lib/rustlib/uninstall.sh
}

std() {
	pkgdesc="Standard library for Rust"
	depends="musl-utils"

	_mv "$pkgdir"$_rlibdir/*.so "$subpkgdir"$_rlibdir

	mkdir -p "$subpkgdir"/etc/ld.so.conf.d
	echo "$_rlibdir" > "$subpkgdir"/etc/ld.so.conf.d/$pkgname.conf
}

analysis() {
	pkgdesc="Compiler analysis data for the Rust standard library"
	depends="$pkgname=$pkgver-r$pkgrel $pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"${_rlibdir%/*}/analysis "$subpkgdir"${_rlibdir%/*}
}

gdb() {
	pkgdesc="GDB pretty printers for Rust"
	license="Apache-2.0 OR MIT"
	depends="$pkgname gdb"
	install_if="$pkgname=$pkgver-r$pkgrel gdb"

	_mv "$pkgdir"/usr/bin/rust-gdb "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/bin/rust-gdbgui "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/share/rust/gdb_*.py "$subpkgdir"/usr/share/rust
}

lldb() {
	pkgdesc="LLDB pretty printers for Rust"
	license="Apache-2.0 OR MIT"
	depends="$pkgname lldb py3-lldb"
	install_if="$pkgname=$pkgver-r$pkgrel lldb"

	_mv "$pkgdir"/usr/bin/rust-lldb "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/share/rust/lldb_*.py "$subpkgdir"/usr/share/rust
}

src() {
	pkgdesc="$pkgdesc (source code)"
	depends=""

	_mv "$pkgdir"/usr/lib/rustlib/src/rust "$subpkgdir"/usr/src
	rmdir -p "$pkgdir"/usr/lib/rustlib/src 2>/dev/null || true

	mkdir -p "$subpkgdir"/usr/lib/rustlib/src
	ln -s ../../../src/rust "$subpkgdir"/usr/lib/rustlib/src/rust
}

cargo() {
	pkgdesc="The Rust package manager"
	provides="cargo-bootstrap=$pkgver-r$pkgrel"
	depends="$pkgname-std=$pkgver-r$pkgrel $pkgname"

	_mv "$pkgdir"/usr/bin/cargo "$subpkgdir"/usr/bin
}

_cargo_clippy() {
	pkgdesc="A collection of Rust lints (cargo plugin)"
	depends="$pkgname-std=$pkgver-r$pkgrel cargo"

	_mv "$pkgdir"/usr/bin/cargo-clippy \
	    "$pkgdir"/usr/bin/clippy-driver \
	    "$subpkgdir"/usr/bin
}

_cargo_fmt() {
	pkgdesc="Format Rust code (cargo plugin)"
	depends="$pkgname-std=$pkgver-r$pkgrel cargo rustfmt"
	install_if="cargo=$pkgver-r$pkgrel rustfmt=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/cargo-fmt "$subpkgdir"/usr/bin
}

_cargo_bashcomp() {
	pkgdesc="Bash completion for cargo"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel bash-completion"

	_mv "$pkgdir"/etc/bash_completion.d/cargo \
	    "$subpkgdir"/usr/share/bash-completion/completions
	rmdir -p "$pkgdir"/etc/bash_completion.d 2>/dev/null || true
}

_cargo_zshcomp() {
	pkgdesc="ZSH completion for cargo"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel zsh"

	_mv "$pkgdir"/usr/share/zsh/site-functions/_cargo \
	    "$subpkgdir"/usr/share/zsh/site-functions/_cargo
	rmdir -p "$pkgdir"/usr/share/zsh/site-functions 2>/dev/null || true
}

_cargo_doc() {
	pkgdesc="The Rust package manager (documentation)"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel docs"

	# XXX: This is hackish!
	_mv "$pkgdir"/../$pkgname-doc/usr/share/man/man1/cargo* \
	    "$subpkgdir"/usr/share/man/man1
}

rustfmt() {
	pkgdesc="Format Rust code"
	provides="rustfmt-bootstrap=$pkgver-r$pkgrel"
	depends="$pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/rustfmt "$subpkgdir"/usr/bin
}

_mv() {
	local dest; for dest; do true; done  # get last argument
	mkdir -p "$dest"
	mv "$@"
}

sha512sums="99d7f276292e5c270648473ff73e9888413a3325ef3a4d7a45f8ce77a42ac87996905f1d875888ce084b621f642017bc9e31a00da1439108dbe19b85d0eab085  rustc-1.79.0-src.tar.xz
ae6ee3084c09f71db09f962dec933802110748f79b1b45e2019667cf43ad2b72489e7b7e5dec16cacdfc009cd14253d855e6bf65f3265a1c3dbd550056d0a733  0001-Fix-LLVM-build.patch
1f976da43d99ff58b7d8716e594485e191c2df43ab4c75123a223ae0cfcca0c129281575d2bee5996faf10f9008357d30c48876a491b62bdd722c83c4f3d35d2  0002-Fix-linking-to-zlib-when-cross-compiling.patch
619a0150bdc59ef8d844e9eff907e51015003164d17012b38bcac642618efb65e25ea4b1ead06d4b023caf02d4eb8dbd481daa04b42d0002a9986f05854c0ca4  0003-Fix-rustdoc-when-cross-compiling-on-musl.patch
66ec46c9c19a7152e8523467eaaf3984d12192a3d4f1c08654027acf0f585317bd59dc5c726f3077f31995ed1503280f4c2263d94fb6a6837d99e8f5797cd1de  0004-Remove-musl_root-and-CRT-fallback-from-musl-targets.patch
6868fe48b7424d39c94ea560037ab4154a0b8f20a027617433d912a5ec60cf0bf1a49da165009345b75f8aedf7d0f81dd23135f9bb28d3f5cd081b56aa74a986  0005-Prefer-libgcc_eh-over-libunwind-for-musl.patch
b34187a82cd3db02b8aac8f18147d653a7191b841e145460dfbff518a7747a6aa1c08721703929bfc7e446c9ed9512383a5eefaad2cb5c02d16e8f00d12627db  0006-Link-libssp_nonshared.a-on-all-musl-targets.patch
e13020274290c1149b7fcf3a119cc34d7f6b64abaa69d6ac9a8780569c4f44041c40b915da6e9675cc7e0ee05d42b13e14608fbab6eac6d3547428bf21f4e9f2  0007-test-failed-doctest-output-Fix-normalization.patch
6850174cebb3dfb2f41e277b0b46db9cabe18c904f39a6775f8cd12dc6c237a7de820a5dc8c538f9b965c069b4197141f0613add096574a7df4ca2d1ae01ab4d  0008-test-sysroot-crates-are-unstable-Fix-test-when-rpath.patch
034149b97ad8db548fcd3deada6ac463a6628900c5ba6cc74148b6eeba662718a9f11b75eb6587abb38720a6578368c47d7a17e28eab519d310ddf9d798e984d  0009-Ignore-broken-and-non-applicable-tests.patch
2ec0e24f8a845cf007eda9c4dd2036ab6d856e96ffc22dc4faa229e1b6c3ebb9dbfc540796aa65b7c8548cf82e95519052dfff5265641f140238bcd63ed18bf8  0010-Link-stage-2-tools-dynamically-to-libstd.patch
4c0d4353ab85354b22d09071020596846acb574f26f768667b96726a4bac2b59c868cff332dca188ad60ba34783cc4b7ba1677836307d63709b35ed9c92818bb  0011-Move-debugger-scripts-to-usr-share-rust.patch
7c86b8a0d363ab08746f28c365c2cec82085f53ee81026a96fffbc03cca356302314d2df5842f73dd12b9803a0f324158c66a99618050df6b23bdba5639da5b2  0012-Add-foxkit-target-specs.patch
5d57a7a18e0a7f8b7755de89255f97642e4fe585a02465de70227867ac99db84766cd25e702ae13c3a4488504bd4c84eb7e9ceb3a83fdf8f2df0eacf64cc16c1  powerpc-atomics.patch"
